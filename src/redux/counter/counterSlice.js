import { createSlice } from '@reduxjs/toolkit';

import React from 'react';

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    value: 0,
  },
  reducers: {
    increment: (state) => {
      state.value += 5;
    },
    decrement: (state) => {
      state.value -= 5;
    },
    customIncrement: (state, action) => {
      state.value += action.payload;
    },
  },
});

export const { increment, decrement, customIncrement } = counterSlice.actions;

export default counterSlice.reducer;
