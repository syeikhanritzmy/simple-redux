import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { customIncrement } from '../redux/counter/counterSlice';

export default function Counter() {
  const count = useSelector((state) => state.counter.value);
  const [value, setValue] = useState('');
  const dispatch = useDispatch();
  return (
    <div>
      <h1>test custom redux</h1>
      {count}
      <input
        type="text"
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <button onClick={() => dispatch(customIncrement(Number(value)))}>
        Add Value
      </button>
    </div>
  );
}
